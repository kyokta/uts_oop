package main.No1_Chart;

import java.util.Arrays;

public class Chart {
    Subject[] listChart = new Subject[10];
    int banyakChart = 0;
    String persentase = "";

    public void addChart(Subject subject){
        listChart[banyakChart] = subject;
        banyakChart++;
    }

    private void setPersentase(){
        int jumlah = 0;
        String persen = "";
        for (int i = 0; i <= banyakChart; i++){
            jumlah += listChart[i].beban;
        }
        for (int i = 0; i <= banyakChart; i++){
            double a = (listChart[i].beban / jumlah) * 100;
            persen += a + '%';
        }
    }

    @Override
    public String toString() {
        return "Chart{" +
                "listChart=" + Arrays.toString(listChart) +
                ", banyakChart=" + banyakChart +
                ", persentase='" + persentase + '\'' +
                '}';
    }
}
