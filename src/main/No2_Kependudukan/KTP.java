package main.No2_Kependudukan;

import static java.lang.Integer.getInteger;
import static java.lang.Integer.parseInt;

public class KTP {
    String nama;
    private String telefon = "088888888888";
    private String tgl_lhr = "01-01-1990";

    public KTP(String nama) {
        this.nama = nama;
    }

    public void setTelefon(String telp){
        this.telefon = telp;
    }

    public void setTgl_lhr(String[] kelahiran){
        String tanggal_lahir = "01-01-1990";
        for (int i = 0; i <= 2; i++){
            int tgl;
            int tanggal = parseInt(kelahiran[0]);
            int bulan = parseInt(kelahiran[1]);
            if (bulan != 2){
                tgl = tanggal;
            } else {
                tgl = 28;
            }
            tanggal_lahir = Integer.toString(tgl) + kelahiran[1] + kelahiran[2];
        }
        this.tgl_lhr = tanggal_lahir;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getTgl_lhr() {
        return tgl_lhr;
    }

    @Override
    public String toString() {
        return "KTP{" +
                "nama='" + nama + '\'' +
                ", telefon='" + getTelefon() + '\'' +
                ", tgl_lhr='" + getTgl_lhr() + '\'' +
                '}';
    }
}
