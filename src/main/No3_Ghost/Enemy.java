package main.No3_Ghost;

public class Enemy {
    private String name;
    private int hp;
    private int attackDamage;

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setHp(int hp){
        this.hp = hp;
    }

    public int getHp(){
        return hp;
    }

    public void setAttackDamage(int ad){
        this.attackDamage = ad;
    }

    public int getAttackDamage(){
        return attackDamage;
    }

}
